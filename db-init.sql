-- create db first on postgresql

-- Table: public.auto_number

-- DROP TABLE public.auto_number;

CREATE TABLE public.auto_number
(
  trx_inc bigint,
  va_inc bigint,
  created_at timestamp with time zone,
  modified_at timestamp with time zone
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.auto_number
  OWNER TO postgres;

-- Table: public.invoice

-- DROP TABLE public.invoice;

CREATE TABLE public.invoice
(
  id bigserial,
  nomor_tagihan character varying(255),
  tipe_tagihan character varying(100), -- IMB/TAGIHAN LAIN/DLL
  detail_tagihan jsonb,
  created_at timestamp with time zone DEFAULT now(),
  modified_at timestamp with time zone,
  created_by character varying(100) DEFAULT 'SYSTEM'::character varying,
  modified_by character varying(100)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.invoice
  OWNER TO postgres;
COMMENT ON COLUMN public.invoice.tipe_tagihan IS 'IMB/TAGIHAN LAIN/DLL';


-- Table: public.invoice_va

-- DROP TABLE public.invoice_va;

CREATE TABLE public.invoice_va
(
  id bigserial,
  invoice_id bigint,
  bank_va character varying(100), -- BNI/MANDIRI/BRI/DLL
  nomor_va character varying(100),
  status_va character varying(100), -- LUNAS/EXPIRED/DLL
  created_at timestamp with time zone DEFAULT now(),
  modifed_at timestamp with time zone,
  created_by character varying(100) DEFAULT 'SYSTEM'::character varying,
  modified_by character varying(100),
  CONSTRAINT invoice_va_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.invoice_va
  OWNER TO postgres;
COMMENT ON COLUMN public.invoice_va.bank_va IS 'BNI/MANDIRI/BRI/DLL';
COMMENT ON COLUMN public.invoice_va.status_va IS 'LUNAS/EXPIRED/DLL';

-- Table: public.va_log

-- DROP TABLE public.va_log;

CREATE TABLE public.va_log
(
  id bigserial,
  request_log text,
  response_log text,
  created_at timestamp with time zone DEFAULT now(),
  modified_at timestamp with time zone
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.va_log
  OWNER TO postgres;
