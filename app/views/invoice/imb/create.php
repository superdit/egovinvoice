<style>
.form-horizontal .control-label{
    /* text-align:right; */
    text-align:left;
}
</style>

<script>
$(function() {
    $("#btn-dummy").click(function(e) {
        e.preventDefault();
        $("input[name=nama]").val("Muhammad bin Sulaiman");
        $("select[name=agama]").val("ISLAM");
        $("select[name=jenis_kelamin]").val("LAKI-LAKI");
        $("select[name=status_pernikahan]").val("MENIKAH");
        $("input[name=tempat_lahir]").val("Balikpapan");
        $("input[name=pekerjaan]").val("Wiraswasta");
        $("textarea[name=alamat]").val("Perumdam BA 72 RT 002 RW 03 \nKelurahan Candirenggo Kecamatan Singosari Kabupaten Malang");
        $("textarea[name=lokasi]").val("Perumahan Villa Bukit Tanjung Kav. 12 RT 001 RW 007 \nDesa Banjararum Kecamatan Singosari Kabupaten Malang");
        $('#datepicker').datepicker('update', new Date("October 21, 1970"));
        $("input[name=luas]").val("1.150 M\u00B2");
        $("input[name=fungsi]").val("Rumah Tinggal Type 48/90");
        $("textarea[name=status_tanah]").val("Akta Pembagian Hak Bersama 1504/2009 Tgl. 30 Maret 2009 An. Kusdianto \n(AJB. No 1464/Kec.Sgs/2009 Tgl. 30-03-2009 An. Muhajir)");
        $("input[name=sebelah_utara]").val("Rumah Kav. 14 Milik Iman Soedjono");
        $("input[name=sebelah_timur]").val("Rumah Milik Kurniati");
        $("input[name=sebelah_selatan]").val("Rumah Kav. 10 Milik Iman Budiono");
        $("input[name=sebelah_barat]").val("Jalan");
        $("input[name=nomor]").val("180/02080/IMB-P/421.302/2013");
        $("input[name=biaya]").val("Rp 1.750.000");
    });

    $("#btn-dummy-reset").click(function(e) {
        e.preventDefault();
        $("input[name=nama]").val("");
        $("select[name=agama]").val("ISLAM");
        $("select[name=jenis_kelamin]").val("LAKI-LAKI");
        $("select[name=status_pernikahan]").val("LAJANG");
        $("input[name=tempat_lahir]").val("");
        $("input[name=pekerjaan]").val("");
        $("textarea[name=alamat]").val("");
        $("textarea[name=lokasi]").val("");
        $('#datepicker').datepicker('update', new Date("October 21, 1970"));
        $("input[name=luas]").val("");
        $("input[name=fungsi]").val("");
        $("textarea[name=status_tanah]").val("");
        $("input[name=sebelah_utara]").val("");
        $("input[name=sebelah_timur]").val("");
        $("input[name=sebelah_selatan]").val("");
        $("input[name=sebelah_barat]").val("");
        $("input[name=nomor]").val("");
        $("input[name=biaya]").val("");
    });

    $('#datepicker').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '01/01/1900',
        autoclose: true
    });
});
</script>

<section class="content-header">
<h1>Buat IMB Baru</h1>
<ol class="breadcrumb">
    <li><a href="<?php echo WEB_URL; ?>dashboard"><i class="fa fa-dashboard"></i> Dasboard</a></li>
    <li><a href="<?php echo WEB_URL; ?>invoice/imb/list">IMB</a></li>
    <li class="active">Buat Baru</li>
</ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
    <div class="col-xs-2"></div>
    <div class="col-xs-8">
        <div class="box box-widget">
            <!-- /.box-header -->

            <form class="form-horizontal" method="post" action="">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-3 control-label"></label>

                  <div class="col-sm-9 text-right">
                    <button class="btn btn-warning btn-sm" id="btn-dummy">Isi Dummy</button>
                    <button class="btn btn-warning btn-sm" id="btn-dummy-reset">Reset Dummy</button>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Nama</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="nama" placeholder="ex: Muhammad bin Sulaiman">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Tempat / Tangggal Lahir</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="tempat_lahir" placeholder="ex: Jakarta">
                  </div>

                  <div class="col-sm-3">
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="tgl_lahir" readonly style="background:#fff;cursor:pointer;" class="form-control pull-right text-center" id="datepicker">
                    </div>
                  </div>

                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Agama</label>

                  <div class="col-sm-9">
                    <select name="agama" class="form-control">
                        <option value="ISLAM">ISLAM</option>
                        <option value="HINDU">HINDU</option>
                        <option value="BUDHA">BUDHA</option>
                        <option value="HINDU">KRISTEN</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">Pekerjaan</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="pekerjaan" placeholder="ex: Wiraswasta">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Alamat</label>

                  <div class="col-sm-9">
                    <textarea class="form-control" name="alamat" placeholder="ex: Perumdam BA 72 RT 002 RW 03 &#10;Kelurahan Candirenggo Kecamatan Singosari Kabupaten Malang"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Jenis Kelamin / Status</label>

                  <div class="col-sm-5">
                    <select name="jenis_kelamin" class="form-control">
                        <option value="LAKI-LAKI">LAKI-LAKI</option>
                        <option value="PEREMPUAN">PEREMPUAN</option>
                    </select>
                  </div>

                  <div class="col-sm-4">
                    <select name="status_pernikahan" class="form-control">
                        <option value="LAJANG">LAJANG</option>
                        <option value="MENIKAH">MENIKAH</option>
                        <option value="CERAI">CERAI</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Lokasi Bangunan</label>

                  <div class="col-sm-9">
                    <textarea class="form-control" name="lokasi" placeholder="ex: Perumahan Villa Bukit Tanjung Kav. 12 RT 001 RW 007 &#10;Desa Banjararum Kecamatan Singosari Kabupaten Malang"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Luas Bangunan</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control meter-persegi" name="luas" placeholder="ex: 48 M&#178;">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Fungsi Bangunan</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="fungsi" placeholder="ex: Rumah Tinggal Type 48/90">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Status Tanah</label>

                  <div class="col-sm-9">
                    <textarea class="form-control" name="status_tanah" placeholder="ex: Akta Pembagian Hak Bersama 1504/2009 Tgl. 30 Maret 2009 An. Kusdianto &#10;(AJB. No 1464/Kec.Sgs/2009 Tgl. 30-03-2009 An. Muhajir)"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Batas Sebelah Utara</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="sebelah_utara" placeholder="ex: Rumah Kav. 14 Milik Iman Soedjono">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Batas Sebelah Timur</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="sebelah_timur" placeholder="ex: Rumah Milik Kurniati">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Batas Sebelah Selatan</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="sebelah_selatan" placeholder="ex: Rumah Kav. 10 Milik Iman Budiono">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Batas Sebelah Barat</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="sebelah_barat" placeholder="ex: Jalan">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Nomor IMB</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="nomor" placeholder="ex: 180/02080/IMB-P/421.302/2013">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Biaya IMB</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control money" name="biaya" placeholder="ex: Rp 1.500.000">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right btn-block"><i class="fa fa-save"></i> &nbsp;Simpan IMB Baru</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
    </div>
    <div class="col-xs-2"></div>
    </div>
</section>