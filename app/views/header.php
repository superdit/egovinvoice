<?php $pver = uniqid(); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="icon" type="image/x-icon" href="<?php echo WEB_URL; ?>theme/img/favicon.ico">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo WEB_URL; ?>theme/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo WEB_URL; ?>theme/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo WEB_URL; ?>theme/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo WEB_URL; ?>theme/dist/css/AdminLTE.min.css">
  <!-- My style -->
  <!-- <link rel="stylesheet" href="<?php echo WEB_URL; ?>theme/dist/css/my.css?v=<?php echo $pver; ?>"> -->
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo WEB_URL; ?>theme/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo WEB_URL; ?>theme/bower_components/select2/dist/css/select2.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <!-- jQuery 3 -->
  <script src="<?php echo WEB_URL; ?>theme/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Moment.js -->
  <script src="<?php echo WEB_URL; ?>theme/bower_components/moment/moment.js"></script>
  <!-- Select2 -->
  <script src="<?php echo WEB_URL; ?>theme/bower_components/select2/dist/js/select2.full.min.js"></script>
  <!-- MaskMoney -->
  <script src="<?php echo WEB_URL; ?>theme/plugins/maskMoney/jquery.maskMoney.min.js"></script>
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo WEB_URL; ?>theme/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <script src="<?php echo WEB_URL; ?>theme/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <!-- date-range-picker -->
  <!-- <link rel="stylesheet" href="<?php echo WEB_URL; ?>theme/bower_components/bootstrap-daterangepicker/daterangepicker.min.css"> -->
  <script src="<?php echo WEB_URL; ?>theme/bower_components/moment/min/moment.min.js"></script>
  <script src="<?php echo WEB_URL; ?>theme/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <!-- bootstrap time picker -->
  <link rel="stylesheet" href="<?php echo WEB_URL; ?>theme/plugins/timepicker/bootstrap-timepicker.min.css">
  <script src="<?php echo WEB_URL; ?>theme/plugins/timepicker/bootstrap-timepicker.min.js"></script>
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-red layout-top-nav fixed">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?php echo WEB_URL; ?>dashboard" class="navbar-brand"><b>E-GOV</b> Invoice</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="<?php if($menu['first'] == 1) echo 'active'; ?>"><a href="<?php echo WEB_URL; ?>dashboard" title="dashboard"><i class="fa fa-dashboard"></i></a></li>
            

            <li class="dropdown <?php if($menu['first'] == 2) echo 'active'; ?>">
              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Tagihan <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo WEB_URL; ?>invoice/imb/list">List IMB</a></li>
                <li><a href="<?php echo WEB_URL; ?>invoice/imb/create"><i class="fa fa-fw fa-plus-square"></i>Buat IMB Baru</a></li>
              </ul>
            </li>
          </ul>
        </div>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown active">
              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-user"></i> &nbsp;<b>Employee 1</b> <small>(ADMINISTRATOR)</small> <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo WEB_URL; ?>auth/logout">Logout</a></li>
              </ul>
            </li>
          </ul>
        </div>

      </div>
      
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->