    <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
      </div>
      <strong>Copyright &copy; <?php echo date('Y'); ?> E-GOV Invoice.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- Admin LTE Select2 CSS Hack -->
<link rel="stylesheet" href="<?php echo WEB_URL; ?>theme/dist/css/select2.hack.min.css">
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo WEB_URL; ?>theme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo WEB_URL; ?>theme/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo WEB_URL; ?>theme/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo WEB_URL; ?>theme/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo WEB_URL; ?>theme/dist/js/demo.js"></script>

<script>
    // apply money masking
    $(".money").maskMoney({
        thousands:'.', 
        decimal:',', 
        precision: 0, 
        allowZero: true, 
        suffix: '', 
        prefix: 'Rp '
    });

    // apply meter persegi
    $(".meter-persegi").maskMoney({
        thousands:'.', 
        decimal:',', 
        precision: 0, 
        allowZero: true, 
        suffix: ' M\u00B2', 
        prefix: ''
    });


    // apply select2
    $('.select2').select2();

    // dropdown menu on hover
    $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(0).fadeIn(100);
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(0).fadeOut(100);
    });
</script>
</body>
</html>
