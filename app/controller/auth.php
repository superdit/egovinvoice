<?php

$app->route('/auth/login', function() use ($db, $msg) {

    $dataOutput = [
        'msg' => $msg
    ];

    app()->render('auth/login.php', $dataOutput);
});