<?php

$app->route('/invoice/imb/create', function() use ($db, $msg) {

    if (request()->method == 'POST')
    {
        // 1. save request log to table va_log
        // e-coll dev
        $client_id = '00084';
        $secret_key = 'f45f9b0da5b88d032cee6ab4b39010f3';
    
        // e-coll prod
        //$client_id = '413';
        //$secret_key = '6e17278732a54bc0eefaba9701eec5e1';

        $amount = (int) str_replace('.', '', str_replace('Rp ', '', data()->biaya));

        $vaRequest = [
            'client_id' => $client_id,
            'trx_id' => '11111',
            'trx_amount' => $amount,
            'billing_type' => 'c',
            'datetime_expired' => '2017-12-05 16:00:00',
            'virtual_account' => '9880008400099999',
            'customer_name' => data()->nama,
            'customer_email' => 'dev_pemda_tangerang@mydomain.com',
            'customer_phone' => '08111222333',
            'description' => 'Biaya IMB Tangerang',
            'type' => 'createbilling'
        ];

        //var_dump(data(), $vaRequest); exit;

        // 2. send request to e-collection

        // 3. save response log to table va_log

        // 4. imb invoice save to table invoice 

        // 5. if va creation success, va number saved to invoice_va

        // 6. if va creation failed, va not saved, invoice imb can create new va
    }

    $dataOutput = [
        
    ];

    renderView($dataOutput, 'invoice/imb/create.php');
});