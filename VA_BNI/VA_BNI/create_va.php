<?php
ini_set('display_errors', 0);

include "resources/setting/Path.inc.php";

try {
    require_once "$_PATH/resources/util/BniEnc.php";
    require_once "$_PATH/services/MemberDao.php";
    
    //$client_id = '00084';
    //$secret_key = 'f45f9b0da5b88d032cee6ab4b39010f3';
	
	$client_id = '413';
    $secret_key = '6e17278732a54bc0eefaba9701eec5e1';
    
    $memberDao = new MemberDao();
    
    //$param = file_get_contents('php://input');
    $jreq = json_decode(json_encode($_POST));

	$noid = $jreq->noid;
    $bank = "BNIS"; 
    $nominal = 0;

    
    //get member
    $member = $memberDao->getMemberByNoid($noid);
    
	$memberVA = "988". $client_id . substr($member->nohp, -8);
	
	$memberVA = "8". $client_id . '00' . substr($member->nohp, -10);

	
    
    //$url = 'https://apibeta.bni-ecollection.com/';
	$url = 'https://api.bni-ecollection.com/';

    $expired  = date('c', mktime(0, 0, 0, date("m"),   date("d"),   date("Y")+100));

    $data_asli = array(
            'client_id' => $client_id,
            'trx_id' => $member->id,
            'trx_amount' => 0,
            'billing_type' => 'o',
            'datetime_expired' => $expired,
            'virtual_account' => $memberVA,
            'customer_name' => $member->nama,
            'customer_email' => $member->email,
            'customer_phone' => $member->nohp,
            'description' => 'Top up '.$noid,
            'type' => 'createbilling'
    );

    $hashed_string = BniEnc::encrypt(
            $data_asli,
            $client_id,
            $secret_key
    );

    $data = array(
        'client_id' => $client_id,
        'data' => $hashed_string,
    );

    $response = BniEnc::get_content($url, json_encode($data));
    $response_json = json_decode($response, true);

    if ($response_json['status'] !== '000') {
        $reply = $response;
    } else {
        $data_response = BniEnc::decrypt($response_json['data'], $client_id, $secret_key);
        $response = array(
            'status' => '0000',
            'bank' => $bank,
            'virtual_account' => $memberVA
        );

        $memberDao->updateVA($noid, $memberVA);
        
        $reply = json_encode($response);
    }
} catch (Exception $ex) {
    $response = array(
        'status' => '0005',
        'message' => $ex->getMessage()
    );
    $reply = json_encode($response);
}

echo urldecode(stripslashes($reply));