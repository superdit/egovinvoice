<?php
ini_set('display_errors', 0);

include "resources/setting/Path.inc.php";

try {
    require_once "$_PATH/resources/util/BniEnc.php";
    require_once "$_PATH/services/MemberDao.php";
        
    //$client_id = '00084';
    //$secret_key = 'f45f9b0da5b88d032cee6ab4b39010f3';
	
	$client_id = '413';
    $secret_key = '6e17278732a54bc0eefaba9701eec5e1';

    // URL utk simulasi pembayaran: http://dev.bni-ecollection.com/


    $data = file_get_contents('php://input');
    $data_json = json_decode($data, true);

    if (!$data_json) {
        echo '{"status":"999","message":"not allowed"}';
    } else {
        if ($data_json['client_id'] === $client_id) {
            $data_asli = BniEnc::decrypt($data_json['data'], $client_id, $secret_key);

            if (!$data_asli) {
                // handling jika waktu server salah/tdk sesuai atau secret key salah
                echo '{"status":"999","message":"waktu server tidak sesuai NTP atau secret key salah."}';
            } else {
                // insert data asli ke db
                /* $data_asli = array(
                        'trx_id' => '', // silakan gunakan parameter berikut sebagai acuan nomor tagihan
                        'virtual_account' => '',
                        'customer_name' => '',
                        'trx_amount' => '',
                        'payment_amount' => '',
                        'cumulative_payment_amount' => '',
                        'payment_ntb' => '',
                        'datetime_payment' => '',
                        'datetime_payment_iso8601' => '',
                ); */
                $idMember = $data_asli['trx_id'];
                $amount = $data_asli['payment_amount'];
                
                $dataTopup = array(
                    'status' => '0000',
                    'bank' => "BNIS",
                    'virtual_account' => $data_asli['virtual_account'],
                    'payment_amount' => $amount,
                    'cumulative_payment_amount' => $data_asli['cumulative_payment_amount'],
                    'customer_name' => $data_asli['customer_name'],
                    'type_payment' => "AUTO TOPUP VA BNIS"
                );
                
                //update saldo member
                $memberDao = new MemberDao();
                $memberDao->updateSaldo($idMember, $amount, json_encode($dataTopup));
				$memberDao->updateMutasi($data_asli['virtual_account'],$amount,$data_asli['cumulative_payment_amount'],$data_asli['customer_name'],$idMember);

                echo '{"status":"000"}';
                exit;
            }
        }
    }
} catch (Exception $ex) {
    echo '{"status":"999","message":'. $ex->getMessage() .'}';
}