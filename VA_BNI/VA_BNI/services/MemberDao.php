<?php

/**
 * @author yudi kurniawan
 * @copyright 2015
 */

require_once "$_PATH/resources/setting/Koneksi.ini.php";

class MemberDao {
    private $koneksi;
    
    function __construct() {
        $this->koneksi = new Koneksi();
    }
    
    function getMemberByNoid($noid) {
        try {
            $pdo = new PDO("pgsql:host=".$this->koneksi->getHost().";port="
                    .$this->koneksi->getPort().";dbname=".$this->koneksi->getDatabase(), 
                    $this->koneksi->getUser(), $this->koneksi->getPassword(), 
                    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            
            $sqlQuery = "SELECT id, noid, nama, alamat, nohp, 
                email, saldo, status  
                FROM member 
                WHERE noid=:noid LIMIT 1";
            $statement = $pdo->prepare($sqlQuery);
            $statement->bindParam(":noid", $noid, PDO::PARAM_STR);
            $statement->execute();
            
            if ($statement->rowCount()>0) {
                $member = $statement->fetch(PDO::FETCH_OBJ);
                
            } else {
                throw new Exception("Data member tidak ditemukan.");
            }
            
            unset($pdo);
            
            return $member;
        } catch (PDOException $ex) {
            throw new Exception("Terjadi kesalahan dalam proses pengambilan data member.".$ex);
        }
    }
    
    function updateSaldo($noid, $nominal, $jsonDetail) {
        try {
            $pdo = new PDO("pgsql:host=".$this->koneksi->getHost().";port="
                    .$this->koneksi->getPort().";dbname=".$this->koneksi->getDatabase(), 
                    $this->koneksi->getUser(), $this->koneksi->getPassword(), 
                    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            
			$id = (int)$noid;
			$nominal = (int)$nominal;
			
            $sqlQuery = "UPDATE member SET last_amount={$nominal},saldo=saldo+{$nominal}, last_trx='{$jsonDetail}' WHERE id={$id}";
            $statement = $pdo->prepare($sqlQuery);
            $statement->execute();
            
            unset($pdo);
        } catch (PDOException $ex) {
            throw new Exception("Terjadi kesalahan dalam proses pengambilan data member.".$ex);
        }
    }
    
    function updateVA($noid, $noVA) {
        try {
            $pdo = new PDO("pgsql:host=".$this->koneksi->getHost().";port="
                    .$this->koneksi->getPort().";dbname=".$this->koneksi->getDatabase(), 
                    $this->koneksi->getUser(), $this->koneksi->getPassword(), 
                    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            
            $sqlQuery = "UPDATE member SET bni=:bni WHERE noid=:noid";
            $statement = $pdo->prepare($sqlQuery);
            $statement->bindParam(":bni", $noVA, PDO::PARAM_STR);
            $statement->bindParam(":noid", $noid, PDO::PARAM_STR);
            $statement->execute();
            
            unset($pdo);
        } catch (PDOException $ex) {
            throw new Exception("Terjadi kesalahan dalam proses pengambilan data member.".$ex);
        }
    }
	
	
	function updateMutasi($va,$amount,$cumulative,$name,$mitra) {
        try {
            $pdo = new PDO("pgsql:host=".$this->koneksi->getHost().";port="
                    .$this->koneksi->getPort().";dbname=".$this->koneksi->getDatabase(), 
                    $this->koneksi->getUser(), $this->koneksi->getPassword(), 
                    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
					
			$nominal = (int)$amount;
			$dc = 'C';
			$saldo = (int)$cumulative;
			$ket = $va . '|'.$amount.'|'.$cumulative.'|'.$name;
			$ket = htmlspecialchars($ket,ENT_QUOTES);
			$mitra = htmlspecialchars(substr($name,0,15),ENT_QUOTES);

            $sqlQuery = "insert into bank_capture(bank,ket,nominal,dc,waktu_bank,saldo,executed,noid)values('VA BNI','{$ket}',{$nominal},'{$dc}',now(),{$saldo},1,'{$mitra}')";
            $statement = $pdo->prepare($sqlQuery);
            $statement->execute();
            
            unset($pdo);
        } catch (PDOException $ex) {
            throw new Exception("Terjadi kesalahan dalam proses pengambilan data member.".$ex);
        }
    }
}
