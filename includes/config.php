<?php

$lifetime = 14400;
session_set_cookie_params($lifetime);
ini_set('session.gc_maxlifetime', $lifetime);
session_name('E6OV8VA');

session_start();
session_regenerate_id();

define('WEB_URL', 'http://localhost:7070/egov-va/');
define('WEB_THEME', WEB_URL . 'theme/');
define('APP_NAME', 'E-Goverment Virtual Account Invoice');
define('JS_VER', uniqid());

define('DBHOST', 'localhost');
define('DBNAME', 'db_egov_va');
define('DBUSER', 'postgres');
define('DBPORT', '5432');
define('DBPASS', 'admin');
define('DOC_ROOT', $_SERVER['DOCUMENT_ROOT'].'/egov-va/');
define('SECURITY_SALT', 'C1829CZMNp9a');