<?php

function setHeaderData($url)
{ 
    $GLOBALS['headerData'] = [
        'title' => '404 Page Not Found &mdash; ' . APP_NAME,
        'menu' => ['first' => 9999999]
    ];

    switch ($url[1])
    {
        case 'dashboard': 
            $GLOBALS['headerData'] = [
                'title' => 'Dashboard &mdash; ' . APP_NAME,
                'menu' => ['first' => 1]
            ];
            break;

        case 'invoice':
            switch ($url[2])
            {
                case 'imb':
                    switch ($url[3])
                    {
                        case 'create':
                            $GLOBALS['headerData'] = [
                                'title' => 'Buat IMB Baru &mdash; ' . APP_NAME,
                                'menu' => ['first' => 4]
                            ];
                            break;
                    }
                break;
            }
            break;

        default: 
            break;
    }
}

function callImage()
{
    require 'includes/libs/php-image-resize/lib/ImageResize.php';    
}

function getSetting($array = false)
{
    $db = new SPdo();

    if ($array)
        return $db->select('*')->from('site_settings')->execBuilder('array')->getRow();
    else
        return $db->select('*')->from('site_settings')->execBuilder()->getRow();
}

function renderView($paramsBody, $urlView)
{
    app()->render('header.php', $GLOBALS['headerData']);
    app()->render($urlView, $paramsBody);
    app()->render('footer.php');
}

function replaceChar($string)
{
    return preg_replace('/[^0-9a-zA-Z_\-\/]/', '_', $string);
}

function getDayName($dayNumber)
{
    $day = '';
    switch ($dayNumber) 
    {
        case 1: $day = 'Monday'; break;
        case 2: $day = 'Tuesday'; break;
        case 3: $day = 'Wednesday'; break;
        case 4: $day = 'Thurday'; break;
        case 5: $day = 'Friday'; break;
        case 6: $day = 'Saturday'; break;
        case 7: $day = 'Sunday'; break;
    }

    return $day;
}

// Returns a file size limit in bytes based on the PHP upload_max_filesize
// and post_max_size
function fileUploadMaxSize() 
{
    static $max_size = -1;

    if ($max_size < 0) 
    {
        // Start with post_max_size.
        $post_max_size = parseSize(ini_get('post_max_size'));
        if ($post_max_size > 0)
            $max_size = $post_max_size;

        // If upload_max_size is less, then reduce. Except if upload_max_size is
        // zero, which indicates no limit.
        $upload_max = parseSize(ini_get('upload_max_filesize'));
        if ($upload_max > 0 && $upload_max < $max_size) 
            $max_size = $upload_max;
    }

    return $max_size;
}

function parseSize($size) 
{
    $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
    $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.

    // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
    if ($unit)
        return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
    else 
        return round($size);
}

function formatBytes($bytes, $precision = 2) 
{ 
    $units = ['B', 'KB', 'MB', 'GB', 'TB']; 

    $bytes = max($bytes, 0); 
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
    $pow = min($pow, count($units) - 1); 

    // Uncomment one of the following alternatives
    // $bytes /= pow(1024, $pow);
    $bytes /= (1 << (10 * $pow)); 

    return round($bytes, $precision) . ' ' . $units[$pow]; 
} 

function sysToMysqlDate($date)
{
    $explDate = explode('/', $date);
    if (count($explDate) > 1)
        return $explDate[2].'/'.$explDate[1].'/'.$explDate[0];
    else
        return '';
}

function mysqlToSysDate($date, $timestamp = false)
{
    $date = substr($date, 0, 10);

    $explDate = explode('-', $date);
    if (count($explDate) > 1)
        return $explDate[2].'/'.$explDate[1].'/'.$explDate[0];
    else
        return '';
}

function encrypt($string, $salt = SECURITY_SALT) 
{
    $encrypt_method = 'AES-256-CBC';
    $secret_key = $salt;
    $secret_iv = 'chiper';

    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    return base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));;
}

function decrypt($string, $salt = SECURITY_SALT) 
{
    $encrypt_method = 'AES-256-CBC';
    $secret_key = $salt;
    $secret_iv = 'chiper';

    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    return openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
}