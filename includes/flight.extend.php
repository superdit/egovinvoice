<?php

function app() 
{ 
    return $GLOBALS['app']; 
}

function request() 
{ 
    return app()->request();
}

$post = app()->request()->data;
$get = app()->request()->query;

foreach ($get as $key => $value)
    $get->{$key} = sanitizeChar($value);

function data() 
{ 
    return app()->request()->data;
}

function query() 
{ 
    global $get;
    return $get;
}

function getMsg()
{
    return $GLOBALS['msg'];
}

function sanitizeChar($string)
{
    return preg_replace('/[^0-9a-zA-Z_\-\/]/', '', $string);
}