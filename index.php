<?php

require 'flight/autoload.php';
require 'includes/config.php';
require 'includes/abstract.pdo.php';

use flight\Engine;
$app = new Engine();
$app->set('flight.views.path', 'app/views');

require 'includes/flight.extend.php';
require 'includes/web.function.php';

$db = new SPdo();

$headerData = [];
$url = explode('/', request()->url);
$control = $url[1];

setHeaderData($url);

$msg = [
    'isError' => false,
    'isSubmit' => false,
    'errorMessage' => '',
    'submitMessage' => ''
];

if ($control != '') 
{
    $filename = 'app/controller/' . $control . '.php';
    
    if (file_exists($filename))
        require $filename;
}

$app->start();